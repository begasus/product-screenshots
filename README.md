Product screenshots for KDE software.

- Git @ `kde:websites/product-screenshots`
- Published @ `https://cdn.kde.org/screenshots/`

Publishing can take an hour or three.

When you have multiple screenshots please create a subdirectory for
your application, also do this if you expect you may want additional
screenshots in the future.

Before pushing please `optipng` your screenshots to make sure they aren't unnecessarily large.

Please note that the CDN only updates on a schedule (> 1 hour).

When creating screenshots for appstream please also check out the appstream documentation:
https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-screenshots
